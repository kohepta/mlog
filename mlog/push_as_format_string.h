﻿#pragma once

#include <string>
#include <algorithm>
#include "convert.h"
#include "util.h"

namespace mlog
{
	template < typename T >
	struct StringTypeHolder
	{
		typedef std::string		Type;
	};

	template <>
	struct StringTypeHolder< std::wstring >
	{
		typedef std::wstring	Type;
	};

	template <>
	struct StringTypeHolder< wchar_t * >
	{
		typedef std::wstring	Type;
	};
	
	template <>
	struct StringTypeHolder< const wchar_t * >
	{
		typedef std::wstring	Type;
	};
	
	inline int FormatStringSize(
		const char * format,
		va_list ap )
	{
		return ::_vscprintf( &format[0], ap );
	}

	inline int FormatStringSize(
		const wchar_t * format,
		va_list ap )
	{
		return ::_vscwprintf( &format[0], ap );
	}

	inline int FormatStringSize(
		const std::string & format,
		va_list ap )
	{
		return ::_vscprintf( &format[0], ap );
	}

	inline int FormatStringSize(
		const std::wstring & format,
		va_list ap )
	{
		return ::_vscwprintf( &format[0], ap );
	}

	inline int FormatStringBuilder(
		char * s,
		size_t c,
		const char * format,
		va_list ap )
	{
		return ::_vsnprintf_s( s, c, _TRUNCATE, &format[0], ap );
	}

	inline int FormatStringBuilder(
		wchar_t * s,
		size_t c,
		const wchar_t * format,
		va_list ap )
	{
		return ::_vsnwprintf_s( s, c, _TRUNCATE, &format[0], ap );
	}

	inline int FormatStringBuilder(
		std::string::pointer s,
		std::string::size_type c,
		const std::string & format,
		va_list ap )
	{
		return ::_vsnprintf_s( s, c, _TRUNCATE, &format[0], ap );
	}

	inline int FormatStringBuilder(
		std::wstring::pointer s,
		std::wstring::size_type c,
		const std::wstring & format,
		va_list ap )
	{
		return ::_vsnwprintf_s( s, c, _TRUNCATE, &format[0], ap );
	}

	template < typename T >
	inline void PushAsFormatStringImpl(
		std::ostringstream & stream,
		const T & str )
	{
		stream << str;
	}

	template <>
	inline void PushAsFormatStringImpl(
		std::ostringstream & stream,
		const std::wstring & str )
	{
		stream << WideToMulti( str );
	}

	template < typename T >
	void PushAsFormatString(
		const T & format,
		std::ostringstream & stream,
		va_list ap )
	{
		int size = FormatStringSize( format, ap );
		
		StringTypeHolder< T >::Type str;
		
		str.resize( size + 1 );
		
		FormatStringBuilder( &str[0], str.size(), format, ap );
		
		str.erase( std::find( str.begin(), str.end(), 0 ), str.end() );

		PushAsFormatStringImpl( stream, str );
	}
}