﻿#pragma once

#include <iostream>
#include "atomic.h"
#include "logger.h"
#include "mutex.h"

namespace mlog 
{
	class DefaultLogger
		: public Logger 
	{
	public:
		explicit DefaultLogger( std::ostream & stream = std::clog )
			: Logger( Info ),
			  stream_( stream ) {}

		virtual void pushRawMessage(
			LogLevel level, 
			const std::string & message ) 
		{
			if ( !checkLevel( level ) ) return;
			
			stream_ << message << std::endl;
		}

		virtual void pushMessage(
			LogLevel level,
			const std::string & message ) 
		{
			if ( !checkLevel( level ) ) return;
			
			pushMessageImpl( level, message );
		}

		virtual void pushAbortMessage( const std::string & message ) 
		{
			pushMessageImpl( Abort, message );
			throw AbortError();
		}

	private:
		void pushMessageImpl(
			LogLevel level,
			const std::string & message ) 
		{
			MutexLock lock( mutex_ );

			OutputLogLevelName( level, stream_ );
			OutputTimestamp( stream_ );
			OutputId( stream_ );
			
			stream_ << message << std::endl;
		}

	private:
		std::ostream & stream_;
		Mutex mutex_;
	};
}
