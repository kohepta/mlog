﻿#pragma once

#include <windows.h>

namespace mlog
{
	inline int CompareAndExchange(
		volatile int & val, 
		int oldval, 
		int newval )
	{
		return ::InterlockedCompareExchange(
			reinterpret_cast< volatile unsigned int * >( &val ), newval, oldval );
	}

	template < typename T >
	inline void * CompareAndExchange(
		T * volatile & val, 
		T * oldval, 
		T * newval )
	{
		void * volatile * ptr = reinterpret_cast< void * volatile * >( &val );
	
		return ::InterlockedCompareExchangePointer( ptr, newval, oldval );
	}

	template < typename T >
	inline bool AtomicallySetImpl(
		volatile T & val, 
		T newval )
	{
		const T oldval = val;

		return CompareAndExchange( val, oldval, newval ) == oldval;
	}

	template < typename T >
	inline void AtomicallySet(
		volatile T & val, 
		T newval ) 
	{
		while ( !AtomicallySetImpl( val, newval ) )
		{
			continue;
		}
	}

	// We wanted to let OnceFlag a POD type.
	typedef volatile int OnceFlag;
	static const OnceFlag kOnceInit = 0;
	static const OnceFlag kOnceCalled = 1;

	template < typename Function >
	inline void CallOnlyOnce(
		OnceFlag & flag, 
		Function function ) 
	{
		if ( CompareAndExchange( flag, kOnceInit, kOnceCalled ) == kOnceInit )
		{
			function();
		}
	}
}