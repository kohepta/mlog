﻿#pragma once

#include <sstream>
#include <string>
#include "logger.h"
#include "logger_holder.h"
#include "push_as_string.h"
#include "push_as_format_string.h"

namespace mlog
{
	template < LogLevel Level = Info >
	class ProxyLogger 
	{
	public:
		explicit ProxyLogger( Logger * logger = NULL )
			: logger_( logger ? *logger : GetCurrentLogger() ) {}

		ProxyLogger( const ProxyLogger & proxy )
			: logger_( proxy.logger_ ) {}

		template < typename T >
		ProxyLogger & operator<<( const T & t )
		{
			PushAsString( t, stream_ );

			return *this;
		}

		template < typename T >
		ProxyLogger & printf(
			const T format,
			... )
		{
			va_list ap;

			va_start( ap, format );

			PushAsFormatString( format, stream_, ap );

			va_end( ap );

			return *this;
		}

		operator std::string() const
		{
			return stream_.str();
		}

		void pushRawMessage() const
		{
			logger_.pushRawMessage( Level, stream_.str() );
		}

		void pushMessage() const 
		{
			logger_.pushMessage( Level, stream_.str() );
		}

	private:
			Logger & logger_;
			std::ostringstream stream_;
	};

	template <>
	inline void ProxyLogger< Abort >::pushMessage() const 
	{
		logger_.pushAbortMessage( stream_.str() );
	}

}
