﻿#pragma once

#include <windows.h>
#include <string>
#include <vector>

namespace mlog
{
	inline std::wstring MultiToWide( const char * str )
	{
		std::wstring wstr;

		const int len = ::MultiByteToWideChar(
			CP_THREAD_ACP,
			0,
			str,
			-1,
			NULL,
			0 );

		if ( len > 0 )
		{
			std::vector< wchar_t > wide( len );
		
			if ( ::MultiByteToWideChar(
				CP_THREAD_ACP,
				0,
				str,
				-1,
				&wide[0],
				len ) )
			{
				std::wstring t( &wide[0] );

				wstr.swap( t );
			}
			else
			{
				wstr = L"";
			}
		}

		return wstr;
	}

	inline std::wstring MultiToWide( const std::string & str )
	{
		return MultiToWide( str.c_str() );
	}

	inline const std::wstring & MultiToWide( const std::wstring & str )
	{
		return str;
	}

	inline std::string WideToMulti( const wchar_t * wstr )
	{
		std::string str;

		const int len = ::WideCharToMultiByte(
			CP_THREAD_ACP,
			0,
			wstr,
			-1,
			NULL,
			0,
			NULL,
			NULL );

		if ( len > 0 )
		{
			std::vector< char > multi( len );
		
			if ( ::WideCharToMultiByte(
				CP_THREAD_ACP,
				0,
				wstr,
				-1,
				&multi[0],
				len,
				NULL,
				NULL ) )
			{
				std::string t( &multi[0] );

				str.swap( t );
			}
			else
			{
				str = "";
			}
		}

		return str;
	}

	inline std::string WideToMulti( const std::wstring & wstr )
	{
		return WideToMulti( wstr.c_str() );
	}

	inline const std::string & WideToMulti( const std::string & str )
	{
		return str;
	}
}