﻿#pragma once

#include <sstream>
#include "atomic.h"
#include "logger.h"
#include "mutex.h"

namespace mlog 
{
	class DebugoutLogger
		: public Logger 
	{
	public:
		DebugoutLogger() : Logger( Info ) {}

		virtual void pushRawMessage(
			LogLevel level, 
			const std::string & message ) 
		{
			if ( !checkLevel( level ) ) return;

			std::ostringstream oss;
			
			oss << message << std::endl;

			::OutputDebugStringA( oss.str().c_str() );
		}

		virtual void pushMessage(
			LogLevel level,
			const std::string & message ) 
		{
			if ( !checkLevel( level ) ) return;
			
			pushMessageImpl( level, message );
		}

		virtual void pushAbortMessage( const std::string & message ) 
		{
			pushMessageImpl( Abort, message );
			throw AbortError();
		}

	private:
		void pushMessageImpl(
			LogLevel level,
			const std::string & message ) 
		{
			std::ostringstream oss;

			OutputLogLevelName( level, oss );
			OutputTimestamp( oss );
			OutputId( oss );

			oss << message << std::endl;

			MutexLock lock( mutex_ );
		
			::OutputDebugStringA( oss.str().c_str() );
		}

	private:
		Mutex mutex_;
	};
}
