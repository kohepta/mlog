﻿#pragma once

namespace mlog
{
	enum WeakSymbol
	{
		Default	
	};

	struct Call
	{
		template < typename Function >
		void operator()( Function function ) const 
		{
			function();
		}
	};
	
	class Noncopyable 
	{
	protected:
		Noncopyable() {}
		~Noncopyable() {}

	private:
		Noncopyable( const Noncopyable & );
		Noncopyable & operator=( const Noncopyable & );
	};

	typedef char TrueType;
	typedef struct { char a[2]; } FalseType;

	template < typename T >
	class IsContainer 
	{
	private:
		template < typename Type >
		static TrueType HasConstIterator( typename Type::const_iterator * );

		template < typename Type >
		static FalseType HasConstIterator( ... );

	public:
		static const bool value = sizeof( HasConstIterator< T >( 0 ) ) == sizeof( TrueType );
	};
}
