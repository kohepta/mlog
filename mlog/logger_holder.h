﻿#pragma once

#include "atomic.h"
#include "singleton.h"
#include "default_logger.h"
#include "util.h"

namespace mlog
{
	class LoggerHolder
		: private Noncopyable 
	{
	public:
		LoggerHolder()
			: logger_( &GetDefaultLogger() ) {}

		Logger & getLogger() const 
		{
			return *logger_;
		}

		void setLogger( Logger & logger )
		{
			AtomicallySet( logger_, &logger );
		}

		void reset() 
		{
			setLogger( GetDefaultLogger() );
		}

	private:
		static Logger & GetDefaultLogger() 
		{
			return Singleton< DefaultLogger >::Get();
		}

	private:
		Logger * volatile logger_;
	};

	inline Logger & GetCurrentLogger() 
	{
		return Singleton< LoggerHolder >::Get().getLogger();
	}

	inline void SetCurrentLogger( Logger & logger ) 
	{
		Singleton< LoggerHolder >::Get().setLogger( logger );
	}

	inline void UseDefaultLogger() 
	{
		Singleton< LoggerHolder >::Get().reset();
	}

	inline void SetDefaultLoggerLevel( LogLevel level ) 
	{
		Singleton< DefaultLogger >::Get().setLevel( level );
	}
}