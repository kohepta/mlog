﻿#pragma once

#include <windows.h>

namespace mlog
{
	class Mutex 
	{
	public:
		Mutex() 
		{
			::InitializeCriticalSection( &cs_ );
		}

		~Mutex()
		{
			::DeleteCriticalSection( &cs_ );
		}

	private:
		void lock()
		{
			::EnterCriticalSection( &cs_ );
		}

		void unlock() 
		{
			::LeaveCriticalSection( &cs_ );
		}

	private:
		CRITICAL_SECTION cs_;

		friend class MutexLock;
	};

	class MutexLock 
	{
	public:
		explicit MutexLock( Mutex & mutex )
			: mutex_( mutex ) 
		{
			mutex_.lock();
		}

		~MutexLock() 
		{
			mutex_.unlock();
		}

	private:
		Mutex & mutex_;
	};
}