﻿#pragma once

#include <cstdlib>
#include <algorithm>
#include <vector>
#include "atomic.h"
#include "mutex.h"
#include "util.h"

namespace mlog
{
	template < WeakSymbol = Default >
	class SingletonFinalizerWeak
	{
	public:
		typedef void ( * Finalizer )();

		static void Register( Finalizer finalizer )
		{
			if ( !finalizers_ || !mutex_ ) { return; }

			MutexLock lock( *mutex_ );

			finalizers_->push_back( finalizer );
		}

		static void Initialize()
		{
			CallOnlyOnce(
				initFlag_, 
				[]()
				{
					finalizers_ = new std::vector< Finalizer >();
					mutex_ = new Mutex();
				});
		}

		static void Finalize() 
		{
			CallOnlyOnce(
				finFlag_,
				[]()
				{
					if ( !finalizers_ || !mutex_ ) { return; }

					while ( !finalizers_->empty() ) 
					{
						const std::vector< Finalizer > finalizers = GetCurrentFinalizers();
					
						std::for_each( finalizers.begin(), finalizers.end(), Call() );
					}

					delete finalizers_;
					delete mutex_;
				});
		}

	private:
		static std::vector< void (*)() > GetCurrentFinalizers()
		{
			std::vector< Finalizer > currentFinalizers;
			
			currentFinalizers.swap( *finalizers_ );
			
			return currentFinalizers;
		}
	
	private:
		static OnceFlag initFlag_;
		static OnceFlag finFlag_;
		static std::vector< Finalizer > * finalizers_;
		static Mutex * mutex_;
	};
	
	template <>
	OnceFlag SingletonFinalizerWeak<>::initFlag_;

	template <>
	OnceFlag SingletonFinalizerWeak<>::finFlag_;

	template <>
	std::vector< typename SingletonFinalizerWeak<>::Finalizer > * SingletonFinalizerWeak<>::finalizers_;

	template <>
	Mutex * SingletonFinalizerWeak<>::mutex_;

	typedef SingletonFinalizerWeak<> SingletonFinalizer;

	template < typename T >
	class Singleton
		: Noncopyable
	{
	public:
		static T & Get()
		{
			CallOnlyOnce(
				initFlag_, 
				[]()
				{
					SingletonFinalizer::Register( Finalize );
					instance_ = new T;
				});
			
			return *instance_;
		}

	protected:
		Singleton() {}

	private:
		static void Finalize() 
		{
			T * instance = instance_;
			instance_ = NULL;

			// More C++ Idioms/Checked delete
			// http://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Checked_delete
			typedef char TypeMustBeComplete[sizeof(T) ? 1 : -1];
			( void ) sizeof( TypeMustBeComplete );

			delete instance;
		}

	private:
		static OnceFlag initFlag_;
		static T * instance_;
	};

	template < typename T >
	OnceFlag Singleton< T >::initFlag_;

	template < typename T >
	T * Singleton< T >::instance_;
}
