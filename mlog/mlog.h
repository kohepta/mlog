﻿#pragma once

#include "proxy_logger.h"
#include "debugout_logger.h"

namespace mlog
{
	struct EmitMessage
	{
		template < typename T >
		void operator<<=( const T & t ) const 
		{
			t.pushMessage();
		}
	};

	struct EmitRawMessage
	{
		template < typename T >
		void operator<<=( const T & t ) const 
		{
			t.pushRawMessage();
		}
	};

	template < typename T >
	T & Ref()
	{
		return Singleton< T >::Get();
	}
}

#define __MLOG_CAT( x, y )							____MLOG_CAT __MLOG_EXPAND((x, y))
#define __MLOG_EXPAND( x )							____MLOG_EXPAND(x)
#define ____MLOG_EXPAND( x )						x
#define ____MLOG_CAT( x, y )						x ## y

#define __MLOG_LPAR (
#define __MLOG_RPAR )

#define __MLOG_TUPLE_LEN( ... )						\
	____MLOG_TUPLE_LEN __MLOG_LPAR _, __VA_ARGS__, 5, 4, 3, 2, 1, 0, _ __MLOG_RPAR

#define ____MLOG_TUPLE_LEN( _0, _1, _2, _3, _4, _5, n, ... ) n

#define __MLOG_OVERLOAD( name, ... )				\
	__MLOG_CAT( name, __MLOG_TUPLE_LEN( __VA_ARGS__ ) )( __VA_ARGS__ )

#define __MLOG_LOG_0()								__MLOG_LOG_1( Info )

#define __MLOG_LOG_1( level )						\
	mlog::EmitMessage() <<=							\
	mlog::ProxyLogger< mlog::level >()

////////////////////////////////////////////////////////////////////////////////

#define MLOG( ... )									__MLOG_OVERLOAD( __MLOG_LOG_, __VA_ARGS__ )

#define MLOF( format, ... )							\
	mlog::EmitMessage() <<=							\
	mlog::ProxyLogger< mlog::Info >().printf( format, __VA_ARGS__ )

#define MLOFL( level, format, ... )					\
	mlog::EmitMessage() <<=							\
	mlog::ProxyLogger< mlog::level >().printf( format, __VA_ARGS__ )

#define MCHK( cond )								\
	( cond ) ? ( void )0 : mlog::EmitMessage() <<=	\
	mlog::ProxyLogger< mlog::Abort >()

#define MINIT()										mlog::SingletonFinalizer::Initialize()
#define MFIN()										mlog::SingletonFinalizer::Finalize()