﻿#pragma once

#include <exception>
#include <string>
#include <windows.h>
#include <iomanip>
#include "util.h"

namespace mlog
{
	enum LogLevel 
	{
		Debug,
		Info,
		Warn,
		Error,
		Fatal,
		Abort
	};

	template < WeakSymbol = Default >
	struct LogLevelNamesWeak 
	{
		static const char * names[6];
	};

	template <>
	const char * LogLevelNamesWeak<>::names[6] = 
	{
		"D",
		"I",
		"W",
		"E",
		"F",
		"A"
	};

	typedef LogLevelNamesWeak<>			LogLevelNames;

	struct AbortError : virtual std::exception {};

	class Logger
	{
	public:
		Logger( LogLevel level )
			: level_( level ) {}

		virtual ~Logger() {}

		template < typename OutputStream >
		static void OutputLogLevelName(
			LogLevel level, 
			OutputStream & stream )
		{
			const char * loglevelname = LogLevelNames::names[level];
			stream << loglevelname << ", ";
		}

		template < typename OutputStream >
		static void OutputTimestamp( OutputStream & stream )
		{
			SYSTEMTIME systim;

			GetLocalTime( &systim );

			stream
				<< std::setw( 4 ) << std::setfill( '0' )
				<< systim.wYear << "/"
				<< std::setw( 2 ) << std::setfill( '0' )
				<< systim.wMonth << "/"
				<< std::setw( 2 ) << std::setfill( '0' )
				<< systim.wDay << ", "
				<< std::setw( 2 ) << std::setfill( '0' )
				<< systim.wHour << ":"
				<< std::setw( 2 ) << std::setfill( '0' )
				<< systim.wMinute << ":"
				<< std::setw( 2 ) << std::setfill( '0' )
				<< systim.wSecond << "."
				<< std::setw( 3 ) << std::setfill( '0' )
				<< systim.wMilliseconds << ", ";
		}

		template < typename OutputStream >
		static void OutputId( OutputStream & stream )
		{
			stream << std::setw( 9 ) << std::setfill( '0' ) << ::GetCurrentProcessId() << ", "
				<< std::setw( 9 ) << std::setfill( '0' ) << ::GetCurrentThreadId() << ", ";
		}

		static std::string OutputLogLevelName( LogLevel level )
		{
			std::ostringstream oss;

			OutputLogLevelName( level, oss );

			return oss.str();
		}

		static std::string OutputTimestamp()
		{
			std::ostringstream oss;
			
			OutputTimestamp( oss );

			return oss.str();
		}

		static std::string OutputId()
		{
			std::ostringstream oss;

			OutputId( oss );

			return oss.str();
		}

		void setLevel( LogLevel level ) 
		{
			level_ = level;
		}

		virtual void pushRawMessage(
			LogLevel level, 
			const std::string & message ) = 0;

		virtual void pushMessage(
			LogLevel level,
			const std::string & message ) = 0;

		virtual void pushAbortMessage( const std::string & message ) = 0;

		inline bool checkLevel( LogLevel level ) 
		{
			return level >= level_;
		}

	private:
		LogLevel level_;
	};
}