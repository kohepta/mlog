﻿#pragma once

#include <sstream>
#include <string>
#include <utility>
#include <vector>
#include "util.h"
#include "convert.h"

namespace mlog
{
	template < typename T, bool IsContainer = IsContainer< T >::value >
	struct StreamBuilder 
	{
		void operator()(
			const T & t, 
			std::ostringstream & stream ) const 
		{
			stream << t;
		}
	};

	template < typename S, typename T >
	struct StreamBuilder< std::pair< S, T >, false >
	{
		void operator()(
			const std::pair< S, T > & t, 
			std::ostringstream & stream ) const 
		{
			stream << '(';
			PushAsString( t.first, stream );
			stream << ',';
			PushAsString( t.second, stream );
			stream << ')';
		}
	};

	template < typename Container >
	struct StreamBuilder< Container, true >
	{
		void operator()(
			const Container & container, 
			std::ostringstream & stream ) const 
		{
			typedef typename Container::const_iterator Iterator;
			Iterator begin = container.begin();
			for ( Iterator itr = container.begin(); itr != container.end(); ++ itr ) 
			{
				if ( itr != begin ) 
				{
					stream << ',';
				}
				
				PushAsString( *itr, stream );
			}
		}
	};

	template <>
	struct StreamBuilder< wchar_t, false >
	{
		void operator()(
			wchar_t str, 
			std::ostringstream & stream ) const 
		{
			stream << WideToMulti( &str );
		}
	};

	template <>
	struct StreamBuilder< wchar_t *, false >
	{
		void operator()(
			wchar_t * str, 
			std::ostringstream & stream ) const 
		{
			stream << WideToMulti( str );
		}
	};

	template <>
	struct StreamBuilder< const wchar_t *, false >
	{
		void operator()(
			const wchar_t * str, 
			std::ostringstream & stream ) const 
		{
			stream << WideToMulti( str );
		}
	};

	template <>
	struct StreamBuilder< std::string, true >
	{
		void operator()(
			const std::string & str, 
			std::ostringstream & stream ) const 
		{
			stream << str;
		}
	};

	template <>
	struct StreamBuilder< std::wstring, true >
	{
		void operator()(
			const std::wstring & str, 
			std::ostringstream & stream ) const 
		{
			stream << WideToMulti( str );
		}
	};

	template < typename T >
	inline void PushAsString(
		const T & t, 
		std::ostringstream & stream )
	{
		//std::cout << typeid( T ).name() << std::endl;

		StreamBuilder< T >()( t, stream );
	}

	template < typename T >
	inline void PushAsString(
		const T * t, 
		std::ostringstream & stream )
	{
		//std::cout << typeid( const T * ).name() << std::endl;
#pragma warning( push )
#pragma warning( disable:4180 )
		StreamBuilder< const T * >()( t, stream );
#pragma warning( pop )
	}

	template < typename T >
	inline void PushAsString(
		T * t, 
		std::ostringstream & stream )
	{
		//std::cout << typeid( T * ).name() << std::endl;

		StreamBuilder< T * >()( t, stream );
	}
}
