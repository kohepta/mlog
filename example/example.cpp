﻿#include <tchar.h>
#include <fstream>
#include <crtdbg.h>
#include "../mlog/mlog.h"

#define _CRTDBG_MAP_ALLOC
#define new		::new( _NORMAL_BLOCK, __FILE__, __LINE__ )

int _tmain(int argc, _TCHAR* argv[])
{
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );

	MINIT();

	// std err
	MLOG() << "standard-info.";
	MLOG( Error ) << "standard-error";

	// output file
	std::ofstream logFile( "test.log", std::ios::trunc );
	mlog::DefaultLogger fileLogger( logFile );
	mlog::SetCurrentLogger( fileLogger );

	MLOG() << "file output (info)";
	MLOG( Error ) << "file output (error)";

	// debug output
	mlog::DebugoutLogger debugoutLogger;
	// 出力レベルを変更
	debugoutLogger.setLevel( mlog::Debug );
	mlog::SetCurrentLogger( debugoutLogger );

	MLOG( Debug ) << "debug out";
	MLOG() << "info out";
	MLOG() << "デバッグメッセージ";
	MLOG() << L"ワイドキャラクタにも対応";
	MLOF( "%d, %s", 1, "printf 的な使い方も可能" );
	MLOF( L"%d, %s", 2, L"wprintf 的な使い方も可能" );

	try
	{
		MLOG( Abort ) << "abort";
	}
	catch ( mlog::AbortError )
	{
		MLOG() << "Abort Error";
	}

	MFIN();

	return 0;
}

